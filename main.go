package main

import (
	"gitlab.com/savoir-faire/rjs-thenewguy/kernel"
	_ "gitlab.com/savoir-faire/rjs-thenewguy/features"
)

func main() {
	connection := kernel.Connection{
		Address: "chat.freenode.net",
		Port: 6667,
		Channels: []string{"#HHNA", "savoir-faire"},
	}

	connection.Connect()
}
