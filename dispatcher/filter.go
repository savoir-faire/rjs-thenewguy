package dispatcher

import "regexp"

type Filter struct {
	FilterString string
	compiledFilter *Regexp
}

func (filter *Filter) MatchMessage(message string) {
	if filter.CompiledFilter == nil {
		filter.CompileFilter()
	}

	filter.compiledFilter.MatchString(message)
}

func (filter *Filter) CompileFilter() {
	filter.compiledFilter = regexp.MustCompile(filter.FilterString)
}
