package dispatcher

import (
	"gitlab.com/savoir-faire/rjs-thenewguy/kernel"
	"gitlab.com/savoir-faire/rjs-thenreguy/pluginmanager"
)

func DispatchMessage(message &kernel.Message) {
	for _, plugin := range pluginmanager.Manager.getPlugins() {
		dispatchToPlugin(message. &plugin)
	}
}

func dispatchToPlugin(message &kernel.Message, plugin &pluginmanager.Plugin) {
	if plugin.Filter.matchMessage(message.Message) {
		plugin.sendMessage(message)
	}
}
