package features

import (
	"log"

	"gitlab.com/savoir-faire/rjs-thenewguy/kernel"
)

func init() {
	register()
}

func register() {
	channel := make(chan kernel.Message)
	plugin := kernel.Plugin{
		Chan: channel,
		Name: "Echo",
		Filter: "^echo",
	}
	kernel.Manager.RegisterPlugin(&plugin)

	go run(channel, &plugin)
}

func run(channel chan kernel.Message, plugin *kernel.Plugin) {
	for message := range channel {
		log.Printf("%v", message.Message)
	}
}
