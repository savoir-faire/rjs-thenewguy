package kernel

import (
	"github.com/belak/irc"
)

type Message struct {
	Connection Connection
	Nick string
	Content string
}

func (message *Message) parseMessage(rawMessage *irc.Message) {
}
