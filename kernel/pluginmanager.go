package kernel

import "log"

var Manager PluginManager = PluginManager{}

type PluginManager struct {
	Plugins []*Plugin
}

type Plugin struct {
	Chan chan Message
	Name string
	Filter string
}

func (manager *PluginManager) RegisterPlugin(plugin *Plugin) {
	log.Printf("Registering plugin %s", plugin.Name)
	manager.Plugins = append(manager.Plugins, plugin)
}
