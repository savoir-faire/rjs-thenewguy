package kernel

import (
	"net"
	"log"
	"fmt"

	"github.com/belak/irc"
	"gitlab.com/savoir-faire/rjs-thenewguy/dispatcher"
)

type Connection struct {
	Channels []string
	Address string
	Port int
}

func (conn *Connection) Connect() (error) {
	log.Println("Opening connection to chat.freenode.net port 6667")
	ircconn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", conn.Address, conn.Port))
	if err != nil {
		return err
	}

	config := irc.ClientConfig{
		Nick: "rjs-thenewguy",
		Pass: "J@spercat",
		User: "rjs-thenewguy",
		Name: "The new guy - Can't you read?",
		Handler: irc.HandlerFunc(conn.dispatch),
	}

	client := irc.NewClient(ircconn, config)
	err = client.Run()
	if err != nil {
		log.Fatalln(err)
	}

	return nil
}

func (conn *Connection) connectToChannels(c * irc.Client) error {
	for _, channel := range conn.Channels {
		log.Printf("Joining channel %s", channel)
		c.Write(fmt.Sprintf("JOIN %s", channel))
	}

	return nil
}

func (conn *Connection) dispatch(c *irc.Client, m *irc.Message) {

	if m.Command == "001" {
		log.Println("Joining channel #HHRA")
		c.Write("JOIN #HNNA")
	} else {
		message := Message{}
		message.Parse(m)

		dispatcher.DispatchMessage(m)
	}
}
